// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "redMonster.h"

//=============================================================================
// default constructor
//=============================================================================
redMonster::redMonster() : Entity()
{
    spriteData.width = redMonsterNS::WIDTH;           // size of Ship1
    spriteData.height = redMonsterNS::HEIGHT;
    spriteData.rect.bottom = redMonsterNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = redMonsterNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    frameDelay = redMonsterNS::RED_MONSTER_ANIMATION_DELAY;
    startFrame = redMonsterNS::RED_MONSTER_IDLE_START;     // first frame of ship animation
	endFrame     = redMonsterNS::RED_MONSTER_IDLE_END;     // last frame of ship animation
	currentFrame = startFrame;
	radius = 17;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	//TODO: use this for damage
	dead = false;
	mass = redMonsterNS::MASS;
	collisionType = entityNS::CIRCLE;
	health = 5;
	deadRedMonster = false;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool redMonster::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
	//TODO: use this for damage
	death.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
	death.setFrames(redMonsterNS::KILL_RED_MONSTER_START, redMonsterNS::KILL_RED_MONSTER_END);
	death.setCurrentFrame(redMonsterNS::KILL_RED_MONSTER_START);
	death.setFrameDelay(redMonsterNS::KILL_RED_MONSTER_ANIMATION_DELAY);
	death.setLoop(false);                  // do not loop animation
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void redMonster::draw()
{
    Image::draw();              // draw ship
	//TODO: use this for damage
	if(dead)
	    // draw shield using colorFilter 50% alpha
	    death.draw(spriteData, 0);
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void redMonster::update(float frameTime)
{
	Entity::update(frameTime);
	spriteData.angle += frameTime * redMonsterNS::ROTATION_RATE;  // rotate the ship
	spriteData.x += frameTime * velocity.x;     // move ship along X 
	spriteData.y += frameTime * velocity.y;     // move ship along Y

	// Bounce off walls
	// if hit right screen edge
	if (spriteData.x > GAME_WIDTH-96-redMonsterNS::WIDTH*getScale())
	{
		// position at right screen edge
		spriteData.x = GAME_WIDTH-96-redMonsterNS::WIDTH*getScale();
		velocity.x = -velocity.x;               // reverse X direction
		audio->playCue(MONSTER_BOUNCE);                  // play sound
	} 
	else if (spriteData.x < 98)                  // else if hit left screen edge
	{
		spriteData.x = 98;                       // position at left screen edge
		velocity.x = -velocity.x;               // reverse X direction
		audio->playCue(MONSTER_BOUNCE);                  // play sound
	}
	// if hit bottom screen edge
	if (spriteData.y > GAME_HEIGHT-101-redMonsterNS::HEIGHT*getScale())
	{
		// position at bottom screen edge
		spriteData.y = GAME_HEIGHT-101-redMonsterNS::HEIGHT*getScale();
		velocity.y = -velocity.y;               // reverse Y direction
		audio->playCue(MONSTER_BOUNCE);                  // play sound
	}
	else if (spriteData.y < 93)                  // else if hit top screen edge
	{
		spriteData.y = 93;                       // position at top screen edge
		velocity.y = -velocity.y;               // reverse Y direction
		audio->playCue(MONSTER_BOUNCE);                  // play sound
	}

	//dying
	if(dead)
    {
        death.update(frameTime);
        if(death.getAnimationComplete())
        {
			dead = false;
            death.setAnimationComplete(false);
			deadRedMonster = true;
        }
    }
}

//=============================================================================
// damage
//=============================================================================
void redMonster::damage(WEAPON weapon)
{
	health--;
	if(health <= 0) {
		dead = true;
	}
}

