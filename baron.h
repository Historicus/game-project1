// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _BARON_H                 // Prevent multiple definitions if this 
#define _BARON_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace baronNS
{
    const int WIDTH = 64;                   // image width
    const int HEIGHT = 64;                  // image height
    const float SPEED = 170.0f;                // 100 pixels per second
    const float MASS = 1000000.0f;              // mass
	const int   TEXTURE_COLS = 3;           // texture has 3 columns
	//BARON actions
	const int BARON_IDLE_START = 0;				
	const int BARON_IDLE_END = 2;

	const int BARON_TOP_SWORD_START = 3;	
	const int BARON_TOP_SWORD_END = 5;

	const int BARON_RIGHT_SWORD_START = 6;	
	const int BARON_RIGHT_SWORD_END = 8;

	const int BARON_DOWN_SWORD_START = 9;	
	const int BARON_DOWN_SWORD_END = 12;

	const int BARON_LEFT_SWORD_START = 12;	
	const int BARON_LEFT_SWORD_END = 14;

	//death
	const int DEATH_START = 36;				
	const int DEATH_END = 38;
	const float DEATH_ANIMATION_DELAY = 0.035f;    // time between frame

	const float BARON_ANIMATION_DELAY = 0.035f;    // time between frames
	const float BARON_IMAGE_SCALE = 1.3f;

	//damage
	const float DAMAGE_ANIMATION_DELAY = 0.08f;    // time between frames
	const int   DAMAGE_START_FRAME = 15;    // damage start frame
	const int   DAMAGE_END_FRAME = 20;      // damage end frame
}

// inherits from Entity class
class Baron : public Entity
{
private:
	//bool and image for when hit and damage given
	bool damageReceived;
	Image baronDamage;

	bool dead;
	Image death;

	bool deadBaron;

	enum LastDirection {left, right, up, down} lastDirection;
	bool keyDownLastFrame;
	bool keyDownThisFrame;
	bool isSwinging;
	int swingDirec;
	int hurt;
	int health;
public:
	// constructor
	Baron();
	int getDirec(){return swingDirec;};
	bool getSwinging(){return isSwinging;};
	float getRadius(){return radius;};
	void setRadius(float r){radius = r;};
	void setHurt(int h){hurt = h;};
	bool getHurt(){return hurt;};

	void setDead(bool dead){deadBaron = dead;};
	bool getDead(){return deadBaron;};

	int checkHealth(){return health;};
    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);
    void damage(WEAPON);
};
#endif

