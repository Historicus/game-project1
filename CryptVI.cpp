// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Draw animated spaceships with collision and shield
// Chapter 6 spacewar.cpp v1.0
// This class is the core of the game

#include "CryptVI.h"
#include <random>
//=============================================================================
// Constructor
//=============================================================================
CryptVI::CryptVI()
{}

//=============================================================================
// Destructor
//=============================================================================
CryptVI::~CryptVI()
{
    releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void CryptVI::initialize(HWND hwnd)
{
    Game::initialize(hwnd); // throws GameError
	//title texture
	if (!titleTexture.initialize(graphics,TITLE_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing title texture"));
	//title image
	if (!title.initialize(graphics,0,0,0,&titleTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing title"));
	title.setX(96);
	title.setY(30);
	title.setScale(0.4f);

	//move texture
	if (!moveTexture.initialize(graphics,MOVE_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing move texture"));
	//move image
	if (!move.initialize(graphics,0,0,0,&moveTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing move"));
	move.setX(5);
	move.setY(350);
	move.setScale(0.09f);

	//sword texture
	if (!swordTexture.initialize(graphics,SWORD_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing sword texture"));
	//swword image
	if (!sword.initialize(graphics,0,0,0,&swordTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing sword"));
	sword.setX(1090);
	sword.setY(325);
	sword.setScale(0.09f);

	//castle texture
	if (!castleTexture.initialize(graphics,CASTLE_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing castle texture"));
	// castle image
    if (!castle.initialize(graphics,0,0,0,&castleTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing castle"));

	//main baron textures
	if (!GameTextures.initialize(graphics, BARON_TEXTURES_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing baron game textures"));

	// baron
	if (!baron.initialize(this, baronNS::WIDTH, baronNS::HEIGHT, baronNS::TEXTURE_COLS, &GameTextures))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing baron"));
	baron.setX(GAME_WIDTH/4-200);                    // start above and left of planet
	baron.setY(GAME_HEIGHT/4-50);
	baron.setFrames(baronNS::BARON_IDLE_START, baronNS::BARON_IDLE_END);   // animation frames
	baron.setCurrentFrame(baronNS::BARON_IDLE_START);     // starting frame
	baron.setFrameDelay(baronNS::BARON_ANIMATION_DELAY);
	baron.setScale(baronNS::BARON_IMAGE_SCALE);

	int temp = 0;
	// other reds
	for(int i = 0; i < 4; i++)
	{
		if (rand()%2 == 0)
			temp = -1;
		else
			temp = 1;
		if (!redMonsters[i].initialize(this, redMonsterNS::WIDTH, redMonsterNS::HEIGHT, redMonsterNS::TEXTURE_COLS, &GameTextures))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing monster " + i));
		redMonsters[i].setX(GAME_WIDTH/2 - rand()%200 + rand()%400);                    // start above and left of planet
		redMonsters[i].setY(GAME_HEIGHT/2 - rand()%400 + rand()%300);
		redMonsters[i].setFrames(redMonsterNS::RED_MONSTER_IDLE_START, redMonsterNS::RED_MONSTER_IDLE_END);   // animation frames
		redMonsters[i].setCurrentFrame(redMonsterNS::RED_MONSTER_IDLE_START);     // starting frame
		redMonsters[i].setFrameDelay(redMonsterNS::RED_MONSTER_ANIMATION_DELAY);
		redMonsters[i].setScale(redMonsterNS::RED_MONSTER_IMAGE_SCALE);
		redMonsters[i].setVelocity(VECTOR2(temp*(redMonsterNS::SPEED + (rand()%50 - 24)),-(redMonsterNS::SPEED + (rand()%50 - 24))));
		redMonsters[i].setDead(false);
	}

	for(int j = 0; j < 6; j++)
	{
		if (rand()%2 == 0)
			temp = -1;
		else
			temp = 1;
		if (!eyeMonsters[j].initialize(this, eyeMonsterNS::WIDTH, eyeMonsterNS::HEIGHT, eyeMonsterNS::TEXTURE_COLS, &GameTextures))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing monster1"));
		eyeMonsters[j].setX(GAME_WIDTH-300-rand()%200);                    // start above and left of planet
		eyeMonsters[j].setY(GAME_HEIGHT-300-rand()%200);
		eyeMonsters[j].setFrames(eyeMonsterNS::EYE_MONSTER_IDLE_START, eyeMonsterNS::EYE_MONSTER_IDLE_END);   // animation frames
		eyeMonsters[j].setCurrentFrame(eyeMonsterNS::EYE_MONSTER_IDLE_START);     // starting frame
		eyeMonsters[j].setFrameDelay(eyeMonsterNS::EYE_MONSTER_ANIMATION_DELAY);
		eyeMonsters[j].setScale(eyeMonsterNS::EYE_MONSTER_IMAGE_SCALE);
		eyeMonsters[j].setVelocity(VECTOR2((eyeMonsterNS::SPEED + (rand()%50 - 24))*temp,-(eyeMonsterNS::SPEED + (rand()%50 - 24)))); // VECTOR2(X, Y)
		eyeMonsters[j].setDead(false);
	}

	//heart
	if (!h1.initialize(this, heartNS::WIDTH, heartNS::HEIGHT, heartNS::TEXTURE_COLS, &GameTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing h1"));
	if (!h2.initialize(this, heartNS::WIDTH, heartNS::HEIGHT, heartNS::TEXTURE_COLS, &GameTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing h2"));
	if (!h3.initialize(this, heartNS::WIDTH, heartNS::HEIGHT, heartNS::TEXTURE_COLS, &GameTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing h3"));
	if (!h4.initialize(this, heartNS::WIDTH, heartNS::HEIGHT, heartNS::TEXTURE_COLS, &GameTextures))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing h4"));
	//h1
	h1.setX(884);                    // start above and left of planet
	h1.setY(690);
	h1.setFrames(heartNS::HEART_START, heartNS::HEART_END);   // animation frames
	h1.setCurrentFrame(heartNS::HEART_START);     // starting frame
	h1.setFrameDelay(heartNS::HEART_ANIMATION_DELAY);
	h1.setScale(heartNS::HEART_IMAGE_SCALE);
	//h2
	h2.setX(932);                    // start above and left of planet
	h2.setY(690);
	h2.setFrames(heartNS::HEART_START, heartNS::HEART_END);   // animation frames
	h2.setCurrentFrame(heartNS::HEART_START);     // starting frame
	h2.setFrameDelay(heartNS::HEART_ANIMATION_DELAY);
	h2.setScale(heartNS::HEART_IMAGE_SCALE);
	//h3
	h3.setX(980);                    // start above and left of planet
	h3.setY(690);
	h3.setFrames(heartNS::HEART_START, heartNS::HEART_END);   // animation frames
	h3.setCurrentFrame(heartNS::HEART_START);     // starting frame
	h3.setFrameDelay(heartNS::HEART_ANIMATION_DELAY);
	h3.setScale(heartNS::HEART_IMAGE_SCALE);
	//h4
	h4.setX(1028);                    // start above and left of planet
	h4.setY(690);
	h4.setFrames(heartNS::HEART_START, heartNS::HEART_END);   // animation frames
	h4.setCurrentFrame(heartNS::HEART_START);     // starting frame
	h4.setFrameDelay(heartNS::HEART_ANIMATION_DELAY);
	h4.setScale(heartNS::HEART_IMAGE_SCALE);

	GameClock = new TextDX();
	if(GameClock->initialize(graphics, 30, true, false, "Calibri")==false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	//win
	win = new TextDX();
	if(win->initialize(graphics, 72, true, false, "Calibri")==false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
	//winSub
	winSub = new TextDX();
	if(winSub->initialize(graphics, 30, true, false, "Calibri")==false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	//lose
	lose = new TextDX();
	if(lose->initialize(graphics, 72, true, false, "Calibri")==false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
	//loseSub
	loseSub = new TextDX();
	if(loseSub->initialize(graphics, 30, true, false, "Calibri")==false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	baron.setDead(false);

	gameOver = false;
	gameOverLose = false;
	gameOverWin = false;


	deathCount = 0;
	return;
}

//=============================================================================
// Update all game items
//=============================================================================
void CryptVI::update()
{
	if(baron.getHurt() > 0)
	{
		baron.setHurt(baron.getHurt() - 1);
	}

	castle.update(frameTime);
	title.update(frameTime);
	move.update(frameTime);
	sword.update(frameTime);

	if (!baron.getDead()){
		baron.update(frameTime);
	} else{
		baron.setX(-50);
		baron.setY(-50);
	}

	int temp1=0, temp2=0;


	//redMonster1.update(frameTime);
	//eyeMonster1.update(frameTime);
	for(int j=0; j < 6; j++)
	{
		if (!eyeMonsters[j].getDead()){
			eyeMonsters[j].update(frameTime);
		} else if (deathCount < 10)
		{
			eyeMonsters[j].setX(GAME_WIDTH/2 + 400);
			eyeMonsters[j].setY(GAME_HEIGHT/2 + 200);
			eyeMonsters[j].setDead(false);
			eyeMonsters[j].heal();
			eyeMonsters[j].setFrames(eyeMonsterNS::EYE_MONSTER_IDLE_START, eyeMonsterNS::EYE_MONSTER_IDLE_END);   // animation frames
			eyeMonsters[j].setCurrentFrame(eyeMonsterNS::EYE_MONSTER_IDLE_START);     // starting frame
			eyeMonsters[j].setFrameDelay(eyeMonsterNS::EYE_MONSTER_ANIMATION_DELAY);
			eyeMonsters[j].setVisible(true);
			deathCount++;
		}
		else
		{
			eyeMonsters[j].setVelocity(VECTOR2(0,0));
			eyeMonsters[j].setX(-25);
			eyeMonsters[j].setY(-25);
			temp1++;
		}
	}

	//reds
	for(int i =0; i < 4; i++)
	{
		if (!redMonsters[i].getDead()) {
			redMonsters[i].update(frameTime);
		} else if (deathCount < 10)
		{
			redMonsters[i].setX(GAME_WIDTH/2 + 400);
			redMonsters[i].setY(GAME_HEIGHT/2 + 200);
			redMonsters[i].setDead(false);
			redMonsters[i].heal();
			redMonsters[i].setFrames(redMonsterNS::RED_MONSTER_IDLE_START, redMonsterNS::RED_MONSTER_IDLE_END);   // animation frames
			redMonsters[i].setCurrentFrame(redMonsterNS::RED_MONSTER_IDLE_START);     // starting frame
			redMonsters[i].setFrameDelay(redMonsterNS::RED_MONSTER_ANIMATION_DELAY);
			redMonsters[i].setVisible(true);
			deathCount++;
		}
		else {
			redMonsters[i].setVelocity(VECTOR2(0,0));
			redMonsters[i].setX(-25);
			redMonsters[i].setY(-25);
			temp2++;
		}
	}
	
	if (temp1+temp2 >= 10){
		gameOver = true;
		gameOverWin = true;
	}


	//hearts
	h1.update(frameTime);
	h2.update(frameTime);
	h3.update(frameTime);
	h4.update(frameTime);

}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void CryptVI::ai()
{}

//=============================================================================
// Handle collisions
//=============================================================================
void CryptVI::collisions()
{
	if(baron.getSwinging())
	{
		baron.setRadius(26);
	}
	else {
		baron.setRadius(14);
	}
	//TODO: collision
    VECTOR2 collisionVector;

	//reds
	for(int i = 0; i < 4; i++)
	{
		if(redMonsters[i].collidesWith(baron, collisionVector))
		{
			if(baron.getSwinging())
			{
				switch(redMonsters[i].getAngle())
				{
				case 1:
					if (baron.getDirec() == 3 || baron.getDirec() == 4)
					{
						redMonsters[i].damage(SWORDSWING);
					}
					else if(baron.getHurt() <= 0)
					{
						baron.damage(MONSTER);
						baron.setHurt(100000000);
			
					}
					break;
				case 2:
					if (baron.getDirec() == 3 || baron.getDirec() == 2)
					{
						redMonsters[i].damage(SWORDSWING);
					}
					else if(baron.getHurt() <= 0)
					{
						baron.damage(MONSTER);
						baron.setHurt(100000000);
			
					}
					break;
				case 3:
					if (baron.getDirec() == 1 || baron.getDirec() == 4)
					{
						redMonsters[i].damage(SWORDSWING);
					}
					else if(baron.getHurt() <= 0)
					{
						baron.damage(MONSTER);
						baron.setHurt(100000000);
			
					}
					break;
				case 4:
					if (baron.getDirec() == 2 || baron.getDirec() == 1)
					{
						redMonsters[i].damage(SWORDSWING);
					}
					else if(baron.getHurt() <= 0)
					{
						baron.damage(MONSTER);
						baron.setHurt(100000000);
			
					}
					break;
				}
			}
			else if(baron.getHurt() <= 0)
			{
				baron.damage(MONSTER);
				baron.setHurt(100000000);
			
			}
			
			//baron.bounce(collisionVector*-1, redMonster1);
			// change the diredction of the collisionVector for ship2
			redMonsters[i].bounce(collisionVector, baron);
			if(redMonsters[i].checkHealth() <= 0)
			{
				redMonsters[i].setVisible(false);
			}
			if(baron.checkHealth() <= 0)
			{
				gameOver = true;
				gameOverLose = true;
				baron.setVisible(false);
			}
		}
	}

	//eyes
	for(int j =0; j < 6; j++)
	{
		if(eyeMonsters[j].collidesWith(baron, collisionVector))
		{
			if(baron.getSwinging())
			{
				switch(eyeMonsters[j].getAngle())
				{
				case 1:
					if (baron.getDirec() == 3 || baron.getDirec() == 4)
					{
						eyeMonsters[j].damage(SWORDSWING);
					}
					else if(baron.getHurt() <= 0)
					{
						baron.damage(MONSTER);
						baron.setHurt(100000000);
			
					}
					break;
				case 2:
					if (baron.getDirec() == 3 || baron.getDirec() == 2)
					{
						eyeMonsters[j].damage(SWORDSWING);
					}
					else if(baron.getHurt() <= 0)
					{
						baron.damage(MONSTER);
						baron.setHurt(100000000);
			
					}
					break;
				case 3:
					if (baron.getDirec() == 1 || baron.getDirec() == 4)
					{
						eyeMonsters[j].damage(SWORDSWING);
					}
					else if(baron.getHurt() <= 0)
					{
						baron.damage(MONSTER);
						baron.setHurt(100000000);
			
					}
					break;
				case 4:
					if (baron.getDirec() == 2 || baron.getDirec() == 1)
					{
						eyeMonsters[j].damage(SWORDSWING);
					}
					else if(baron.getHurt() <= 0)
					{
						baron.damage(MONSTER);
						baron.setHurt(100000000);
			
					}
					break;
				}
			}
			else if(baron.getHurt() <= 0)
			{
				baron.damage(MONSTER);
				baron.setHurt(100000000);
			
			}
			
			// bounce off ship
			//baron.bounce(collisionVector*-1, redMonster1);
			// change the diredction of the collisionVector for ship2
			eyeMonsters[j].bounce(collisionVector, baron);
			if(eyeMonsters[j].checkHealth() <= 0)
			{
				eyeMonsters[j].setVisible(false);
			}
			if(baron.checkHealth() <= 0)
			{
				gameOver = true;
				gameOverLose = true;
				baron.setVisible(false);
			}

		}
	}

	int tempHealth = baron.checkHealth();
	switch (tempHealth)
	{
		case 3:
			h1.setVisible(false);
			break;
		case 2:
			h2.setVisible(false);
			break;
		case 1:
			h3.setVisible(false);
			break;
		case 0:
			h4.setVisible(false);
			break;
		default:
			break;
	}
}

//=============================================================================
// Render game items
//=============================================================================
void CryptVI::render()
{
	graphics->spriteBegin();                // begin drawing sprites
	castle.draw();
	title.draw();
	move.draw();
	sword.draw();

	if (!baron.getDead())
		baron.draw();

	for(int j=0; j < 6; j++)
	{
		if (!eyeMonsters[j].getDead())
			eyeMonsters[j].draw();
	}

	//reds
	for(int i = 0; i < 4; i ++)
	{
		if (!redMonsters[i].getDead())
			redMonsters[i].draw();
	}

	//hearts
	h1.draw();
	h2.draw();
	h3.draw();
	h4.draw();

	if (!gameOver) {
		std::stringstream timer;
		timer << "TIMER:  ";
		timer << clock()/CLOCKS_PER_SEC;
		countdown= clock()/CLOCKS_PER_SEC;
		GameClock->print(timer.str(), 930, 65);
	}
	if(gameOverLose)
	{
		std::stringstream loser;
		std::stringstream losersub;
		loser << "YOU LOSE";
		losersub << "Your time: ";
		losersub << countdown << " secs";

		std::stringstream timer;
		timer << "TIMER:  ";
		timer << countdown;
		GameClock->print(timer.str(), 930, 65);

		//lose screen without baron
		lose->print(loser.str(),485 , 250);
		loseSub->print(losersub.str(),520 ,320);
	}
	if (gameOverWin){

		std::stringstream winner;
		std::stringstream winnersub;
		winner << "YOU WIN";
		winnersub << "Your time: ";
		winnersub << countdown << " secs";
		
		std::stringstream timer;
		timer << "TIMER:  ";
		timer << countdown;
		GameClock->print(timer.str(), 930, 65);

		//win screen without mosnters
		win->print(winner.str(),490 , 250);
		winSub->print(winnersub.str(),520 ,320);
	}

    graphics->spriteEnd();                  // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void CryptVI::releaseAll()
{
	
	GameTextures.onLostDevice();
	castleTexture.onLostDevice();
	titleTexture.onLostDevice();
	swordTexture.onLostDevice();
	moveTexture.onLostDevice();

    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void CryptVI::resetAll()
{
	GameTextures.onResetDevice();
	castleTexture.onResetDevice();
	titleTexture.onResetDevice();
	swordTexture.onResetDevice();
	moveTexture.onResetDevice();

    Game::resetAll();
    return;
}
