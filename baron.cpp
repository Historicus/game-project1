// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "baron.h"

//=============================================================================
// default constructor
//=============================================================================
Baron::Baron() : Entity()
{
    spriteData.width = baronNS::WIDTH;           // size of Ship1
    spriteData.height = baronNS::HEIGHT;
    spriteData.rect.bottom = baronNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = baronNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    frameDelay = baronNS::BARON_ANIMATION_DELAY;
    startFrame = baronNS::BARON_IDLE_START;     // first frame of ship animation
    endFrame     = baronNS::BARON_IDLE_END;     // last frame of ship animation
    currentFrame = startFrame;
    radius = 14; //26 when sword is out
	damageReceived = false;
	dead = false;
    mass = baronNS::MASS;
	collisionType = entityNS::CIRCLE;
	isSwinging = false;
	hurt = 0;
	health = 4;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool Baron::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
	//TODO: initialize the image that is being used for damage
    baronDamage.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
    baronDamage.setFrames(baronNS::DAMAGE_START_FRAME, baronNS::DAMAGE_END_FRAME);
    baronDamage.setCurrentFrame(baronNS::DAMAGE_START_FRAME);
    baronDamage.setFrameDelay(baronNS::DAMAGE_ANIMATION_DELAY);
	baronDamage.setLoop(false);

	death.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
	death.setFrames(baronNS::DEATH_START, baronNS::DEATH_END);
	death.setCurrentFrame(baronNS::DEATH_START);
	death.setFrameDelay(baronNS::DEATH_ANIMATION_DELAY);
	death.setLoop(false);                  // do not loop animation

	lastDirection = right;
	keyDownLastFrame = false;
	keyDownThisFrame = false;

	bool deadBaron = false;
	return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void Baron::draw()
{
	Image::draw();

	if((damageReceived)&&(!dead))
		baronDamage.draw(spriteData, graphicsNS::ALPHA50 & colorFilter);

	if (dead)
		death.draw(spriteData, 0);
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Baron::update(float frameTime)
{
	Entity::update(frameTime);
	//=======================================================================
	//								MOVEMENT
	//=======================================================================
	int directionX = 0;
	int directionY = 0;

	if(input->isKeyDown(BARON_RIGHT_KEY))            // if move right
	{
		directionX++;
	}
	if(input->isKeyDown(BARON_LEFT_KEY))             // if move left
	{
		directionX--;
	}
	if(input->isKeyDown(BARON_UP_KEY))               // if move up
	{
		directionY--;
	}
	if(input->isKeyDown(BARON_DOWN_KEY))             // if move down
	{
		directionY++;
	}

	//=======================================================================
	//							KEYBOARD INPUT: SWORD
	//=======================================================================
	if(input->isKeyDown(SWORD_RIGHT))            // if move right
	{
		keyDownThisFrame = true;
		lastDirection= right;
		isSwinging = true;
		swingDirec = 1;
	}
	if(input->isKeyDown(SWORD_LEFT))             // if move left
	{
		lastDirection = left;
		keyDownThisFrame = true;
		isSwinging = true;
		swingDirec = 3;
	}
	if(input->isKeyDown(SWORD_UP))               // if move up
	{
		lastDirection = up;
		keyDownThisFrame = true;
		isSwinging = true;
		swingDirec = 4;
	}
	if(input->isKeyDown(SWORD_DOWN))             // if move down
	{
		lastDirection = down;
		keyDownThisFrame = true;
		isSwinging = true;
		swingDirec = 2;
	}
	if (!keyDownLastFrame && keyDownThisFrame)
	{
		switch (lastDirection)
		{
		case right:
			startFrame = baronNS::BARON_RIGHT_SWORD_START;
			endFrame = baronNS::BARON_RIGHT_SWORD_END;

			isSwinging = true;

			audio->playCue(SWORD);
			break;
		case left:
			startFrame = baronNS::BARON_LEFT_SWORD_START;
			endFrame = baronNS::BARON_LEFT_SWORD_END;

			isSwinging = true;

			audio->playCue(SWORD);
			break;
		case up:
			startFrame = baronNS::BARON_TOP_SWORD_START;
			endFrame = baronNS::BARON_TOP_SWORD_END;

			isSwinging = true;

			audio->playCue(SWORD);
			break;
		case down:
			startFrame = baronNS::BARON_DOWN_SWORD_START;
			endFrame = baronNS::BARON_DOWN_SWORD_END;

			isSwinging = true;

			audio->playCue(SWORD);
			break;
		}
	}

	if (!keyDownThisFrame)
	{
		startFrame = baronNS::BARON_IDLE_START;
		endFrame = baronNS::BARON_IDLE_END;

		isSwinging = false;
	}

	keyDownLastFrame = keyDownThisFrame;
	keyDownThisFrame = false;

	//taking damage
	if(damageReceived)
    {
        baronDamage.update(frameTime);
        if(baronDamage.getAnimationComplete())
        {
            damageReceived = false;
			baronDamage.setAnimationComplete(false);
			baronDamage.setCurrentFrame(baronNS::DAMAGE_START_FRAME);
		}
	}

	//dying
	if(dead)
	{
		death.update(frameTime);
		if(death.getAnimationComplete())
		{
			dead = false;
			death.setAnimationComplete(true);
			baronDamage.setCurrentFrame(baronNS::DEATH_START);
			deadBaron = true;
		}
	}

	//=======================================================================
	//								Screenedge
	//=======================================================================
	float tempX = spriteData.x + directionX * frameTime * baronNS::SPEED;
	float tempY = spriteData.y + directionY * frameTime * baronNS::SPEED;

	if ((tempX + spriteData.width*baronNS::BARON_IMAGE_SCALE) >= GAME_WIDTH-92 || tempX <= 94)
		directionX = 0;
	if ((tempY + spriteData.height*baronNS::BARON_IMAGE_SCALE) >= GAME_HEIGHT-97 || tempY <= 89)
		directionY = 0;
	spriteData.x = spriteData.x + directionX * frameTime * baronNS::SPEED;
	spriteData.y = spriteData.y + directionY * frameTime * baronNS::SPEED;
}

//=============================================================================
// damage
//=============================================================================
void Baron::damage(WEAPON weapon)
{
	if(health > 0 && hurt <= 0)
		health--;
	if (health <= 0)
		dead = true;

	//taking damage
	damageReceived = true;
}

