// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _HEART_H                 // Prevent multiple definitions if this 
#define _HEART_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace heartNS
{
    const int WIDTH = 64;                   // image width
    const int HEIGHT = 64;                  // image height
	const int   TEXTURE_COLS = 3;           // texture has 8 columns
	//BARON actions
	const int HEART_START = 21;				
	const int HEART_END = 21;

	const int HALF_HEART_START = 22;				
	const int HALF_HEART_END = 22;
	
	const float HEART_ANIMATION_DELAY = 0.035f;    // time between frames
	const float HEART_IMAGE_SCALE = 1.1f;
}

// inherits from Entity class
class heart : public Entity
{
private:
public:
    // constructor
    heart();

    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);
    void damage(WEAPON);
};
#endif

