// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 5 spacewar.h v1.0

#ifndef _SPACEWAR_H             // Prevent multiple definitions if this 
#define _SPACEWAR_H             // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include <string>

//=============================================================================
// This class is the core of the game
//=============================================================================
class Spacewar : public Game
{
private:

	#define BARON_IMAGE_SCALE 1.1
	TextureManager cryptTexture;
	Image crypt;

	enum LastDirection {left, right, up, down} lastDirection;
	bool keyDownLastFrame;
	bool keyDownThisFrame;

	int stop;

public:
    // Constructor
    Spacewar();

    // Destructor
    virtual ~Spacewar();

    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
};

#endif
