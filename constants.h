// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 constants.h v1.0

#ifndef _CONSTANTS_H            // Prevent multiple definitions if this 
#define _CONSTANTS_H            // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//-----------------------------------------------
// Useful macros
//-----------------------------------------------
// Safely delete pointer referenced item
#define SAFE_DELETE(ptr)       { if (ptr) { delete (ptr); (ptr)=NULL; } }
// Safely release pointer referenced item
#define SAFE_RELEASE(ptr)      { if(ptr) { (ptr)->Release(); (ptr)=NULL; } }
// Safely delete pointer referenced array
#define SAFE_DELETE_ARRAY(ptr) { if(ptr) { delete [](ptr); (ptr)=NULL; } }
// Safely call onLostDevice
#define SAFE_ON_LOST_DEVICE(ptr)    { if(ptr) { ptr->onLostDevice(); } }
// Safely call onResetDevice
#define SAFE_ON_RESET_DEVICE(ptr)   { if(ptr) { ptr->onResetDevice(); } }
#define TRANSCOLOR  SETCOLOR_ARGB(0,255,0,255)  // transparent color (magenta)

//-----------------------------------------------
//                  Constants
//-----------------------------------------------

// window
const char CLASS_NAME[] = "CryptVI";
const char GAME_TITLE[] = "CryptVI";
const bool FULLSCREEN = false;              // windowed or fullscreen
const UINT GAME_WIDTH =  1200;               // width of game in pixels
const UINT GAME_HEIGHT = 800;               // height of game in pixels

// game
const double PI = 3.14159265;
const float FRAME_RATE = 200.0f;                // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;             // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;   // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time used in calculations
const float GRAVITY = 6.67428e-11f;             // gravitational constant
const float MASS_PLANET = 1.0e14f;
const float MASS_SHIP = 5.0f;

// audio cues
//need to be same as cue names in XACT
// audio files required by audio.cpp
// WAVE_BANK must be location of .xwb file.
const char WAVE_BANK[]  = "audio\\Win\\CryptWaveBank.xwb";
// SOUND_BANK must be location of .xsb file.
const char SOUND_BANK[] = "audio\\Win\\CryptSoundBank.xsb";

// audio cues
//need to be same as cue names in XACT
const char SWORD[] = "sword";
const char MONSTER_BOUNCE[] = "monsterBounce";
const char SLIME_SWORD[] = "slimeSword";

// graphic images
const char MOVE_IMAGE[] = "pictures\\move.png";
const char SWORD_IMAGE[] = "pictures\\sword.png";
const char TITLE_IMAGE[] = "pictures\\title.png";
const char CASTLE_IMAGE[] =   "pictures\\castle.png";     // photo source https://freestocktextures.com
const char BARON_TEXTURES_IMAGE[] = "pictures\\Baron.png";  // baron game textures, NicoleMarieProductions for heart

// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR ESC_KEY      = VK_ESCAPE;       // escape key
const UCHAR ALT_KEY      = VK_MENU;         // Alt key
const UCHAR ENTER_KEY    = VK_RETURN;       // Enter key
const UCHAR BARON_LEFT_KEY    = 'A';    // A key
const UCHAR BARON_RIGHT_KEY   = 'D';    // D key
const UCHAR BARON_UP_KEY      = 'W';    // W key
const UCHAR BARON_DOWN_KEY    = 'S';    // S key
const UCHAR SWORD_LEFT    = VK_LEFT;     // left arrow
const UCHAR SWORD_RIGHT   = VK_RIGHT;    // right arrow
const UCHAR SWORD_UP      = VK_UP;       // up arrow
const UCHAR SWORD_DOWN    = VK_DOWN;     // down arrow

// weapon types
enum WEAPON {SWORDSWING, MONSTER, SPIKES};

#endif
