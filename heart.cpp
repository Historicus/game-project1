// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "heart.h"

//=============================================================================
// default constructor
//=============================================================================
heart::heart() : Entity()
{
    spriteData.width = heartNS::WIDTH;           // size of Ship1
    spriteData.height = heartNS::HEIGHT;
    spriteData.rect.bottom = heartNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = heartNS::WIDTH;
    frameDelay = heartNS::HEART_ANIMATION_DELAY;
    startFrame = heartNS::HEART_START;     // first frame of ship animation
    endFrame     = heartNS::HEART_END;     // last frame of ship animation
    currentFrame = startFrame;
    radius = heartNS::WIDTH/2.0;

	collisionType = entityNS::CIRCLE;
}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool heart::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void heart::draw()
{
    Image::draw();              // draw ship
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void heart::update(float frameTime)
{
    Entity::update(frameTime);
}

//=============================================================================
// damage
//=============================================================================
void heart::damage(WEAPON weapon)
{
}

