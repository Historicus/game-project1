// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.cpp v1.0

#include "eyeMonster.h"

//=============================================================================
// default constructor
//=============================================================================
eyeMonster::eyeMonster() : Entity()
{
    spriteData.width = eyeMonsterNS::WIDTH;           // size of Ship1
    spriteData.height = eyeMonsterNS::HEIGHT;
    spriteData.rect.bottom = eyeMonsterNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = eyeMonsterNS::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    frameDelay = eyeMonsterNS::EYE_MONSTER_ANIMATION_DELAY;
    startFrame = eyeMonsterNS::EYE_MONSTER_IDLE_START;     // first frame of ship animation
	endFrame     = eyeMonsterNS::EYE_MONSTER_IDLE_END;     // last frame of ship animation
	currentFrame = startFrame;
	radius = 23;
	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y
	//TODO: use this for damage
	//shieldOn = false;
	mass = eyeMonsterNS::MASS;
	collisionType = entityNS::CIRCLE;
	health = 5;

	dead = false;
	deadEyeMonster = false;

}

//=============================================================================
// Initialize the Ship.
// Post: returns true if successful, false if failed
//=============================================================================
bool eyeMonster::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
	death.initialize(gamePtr->getGraphics(), width, height, ncols, textureM);
	death.setFrames(eyeMonsterNS::KILL_EYE_MONSTER_START, eyeMonsterNS::KILL_EYE_MONSTER_END);
	death.setCurrentFrame(eyeMonsterNS::KILL_EYE_MONSTER_START);
	death.setFrameDelay(eyeMonsterNS::KILL_EYE_MONSTER_ANIMATION_DELAY);
	death.setLoop(false);                  // do not loop animation
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// draw the ship
//=============================================================================
void eyeMonster::draw()
{
    Image::draw();              // draw eyeMonster
	if(dead)
	    // draw shield using colorFilter 50% alpha
	    death.draw(spriteData, 0);
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void eyeMonster::update(float frameTime)
{
	Entity::update(frameTime);
	spriteData.angle += frameTime * eyeMonsterNS::ROTATION_RATE;  // rotate the ship
	spriteData.x += frameTime * velocity.x;     // move ship along X 
	spriteData.y += frameTime * velocity.y;     // move ship along Y

	// Bounce off walls
	// if hit right screen edge
	if (spriteData.x > GAME_WIDTH-100-eyeMonsterNS::WIDTH*getScale())
	{
		// position at right screen edge
		spriteData.x = GAME_WIDTH-100-eyeMonsterNS::WIDTH*getScale();
		velocity.x = -velocity.x;               // reverse X direction
		audio->playCue(MONSTER_BOUNCE);                  // play sound
	} 
	else if (spriteData.x < 103)                  // else if hit left screen edge
	{
		spriteData.x = 103;                       // position at left screen edge
		velocity.x = -velocity.x;               // reverse X direction
		audio->playCue(MONSTER_BOUNCE);                  // play sound
	}
	// if hit bottom screen edge
	if (spriteData.y > GAME_HEIGHT-105-eyeMonsterNS::HEIGHT*getScale())
	{
		// position at bottom screen edge
		spriteData.y = GAME_HEIGHT-105-eyeMonsterNS::HEIGHT*getScale();
		velocity.y = -velocity.y;               // reverse Y direction
		audio->playCue(MONSTER_BOUNCE);                  // play sound
	}
	else if (spriteData.y < 97)                  // else if hit top screen edge
	{
		spriteData.y = 97;                       // position at top screen edge
		velocity.y = -velocity.y;               // reverse Y direction
		audio->playCue(MONSTER_BOUNCE);                  // play sound
	}
	//dying
	if(dead)
	{
		death.update(frameTime);
		if(death.getAnimationComplete())
		{
			dead = false;
			death.setAnimationComplete(false);
			deadEyeMonster = true;
		}
	}
}

//=============================================================================
// damage
//=============================================================================
void eyeMonster::damage(WEAPON weapon)
{
	health--;
	if(health <= 0) {
		dead = true;
	}
}

