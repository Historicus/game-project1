// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 spacewar.h v1.0

#ifndef _CRYPTVI_H             // Prevent multiple definitions if this 
#define _CRYPTVI_H             // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"

//sprites
#include "baron.h"
#include "redMonster.h"
#include "eyeMonster.h"
#include "heart.h"

//text
#include "textDX.h"
#include <string>
#include <sstream>

#include <time.h>

//=============================================================================
// This class is the core of the game
//=============================================================================
class CryptVI : public Game
{
private:
    // game items
	TextureManager titleTexture; //title texture
	TextureManager moveTexture;
	TextureManager swordTexture;
	TextureManager castleTexture; //castle texture
	TextureManager GameTextures; //crypt game texture 
	Image title;
	Image move;
	Image sword;
	Image castle;
	Baron baron;
	redMonster redMonsters[4];
	redMonster redMonster1;
	eyeMonster eyeMonsters[6];
	eyeMonster eyeMonster1;
	heart h1,h2,h3,h4;

	bool gameOver;
	bool gameOverLose;
	bool gameOverWin;

	TextDX *GameClock;
	TextDX *win;
	TextDX *winSub;
	TextDX *lose;
	TextDX *loseSub;

	int deathCount;

	float countdown;
public:
    // Constructor
    CryptVI();

    // Destructor
    virtual ~CryptVI();

    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
};

#endif
