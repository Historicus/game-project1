// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#ifndef _EYEMONSTER_H                 // Prevent multiple definitions if this 
#define _EYEMONSTER_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace eyeMonsterNS
{
    const int WIDTH = 64;                   // image width
    const int HEIGHT = 64;                  // image height
    const float SPEED = 125.0f;                // 100 pixels per second
    const float MASS = 100.0f;              // mass
	const int   TEXTURE_COLS = 3;           // texture has 8 columns
	const float ROTATION_RATE = (float)PI/4; // radians per second
	//EYEMONSTER actions
	const int EYE_MONSTER_IDLE_START = 30;				
	const int EYE_MONSTER_IDLE_END = 30;
	const int KILL_EYE_MONSTER_START = 30;				
	const int KILL_EYE_MONSTER_END = 35;
	const float KILL_EYE_MONSTER_ANIMATION_DELAY = 0.08f;    // time between frames

	const float EYE_MONSTER_ANIMATION_DELAY = 0.035f;    // time between frames
	const float EYE_MONSTER_IMAGE_SCALE = 1.1f;
}

// inherits from Entity class
class eyeMonster : public Entity
{
private:
	//dying
	bool dead;
	Image death;

	bool deadEyeMonster;

	int health;
public:
    // constructor
    eyeMonster();
	int checkHealth(){return health;};
	void heal(){health = 5;};

    // inherited member functions
    virtual void draw();
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
	void setDead(bool dead){deadEyeMonster = dead;};
	bool getDead(){return deadEyeMonster;};

    void update(float frameTime);
    void damage(WEAPON);
};
#endif

