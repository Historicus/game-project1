// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Move spacejpo with arrow keys.
// Chapter 5 spacewar.cpp v1.0
// This class is the core of the game

#include "spaceWar.h"

//=============================================================================
// Constructor
//=============================================================================
Spacewar::Spacewar()
{}

//=============================================================================
// Destructor
//=============================================================================
Spacewar::~Spacewar()
{
    releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Spacewar::initialize(HWND hwnd)
{
    Game::initialize(hwnd); // throws GameError
	if (!cryptTexture.initialize(graphics,BARON_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing crypt texture"));

	if (!crypt.initialize(graphics,CRYPT_WIDTH, CRYPT_HEIGHT, CRYPT_COLS, &cryptTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing crypt"));
	crypt.setX(GAME_WIDTH/4);                    // start above and left of planet
	crypt.setY(GAME_HEIGHT/4);
	crypt.setFrames(CRYPT_IDLE_START, CRYPT_IDLE_END);   // animation frames
	crypt.setCurrentFrame(CRYPT_IDLE_START);     // starting frame
	crypt.setFrameDelay(CRYPT_ANIMATION_DELAY);
	crypt.setScale(BARON_IMAGE_SCALE);

	lastDirection = right;
	keyDownLastFrame = false;
	keyDownThisFrame = false;

	stop = 1;

    return;
}

//=============================================================================
// Update all game items
//=============================================================================
void Spacewar::update()
{
	//=======================================================================
	//								MOVEMENT
	//=======================================================================
	if(input->isKeyDown(CRYPT_RIGHT_KEY))            // if move right
	{
		crypt.setX(crypt.getX() + frameTime * CRYPT_SPEED);
	}
	if(input->isKeyDown(CRYPT_LEFT_KEY))             // if move left
	{
		crypt.setX(crypt.getX() - frameTime * CRYPT_SPEED);
	}
	if(input->isKeyDown(CRYPT_UP_KEY))               // if move up
	{
		crypt.setY(crypt.getY() - frameTime * CRYPT_SPEED);
	}
	if(input->isKeyDown(CRYPT_DOWN_KEY))             // if move down
	{
		crypt.setY(crypt.getY() + frameTime * CRYPT_SPEED);
	}

	//=======================================================================
	//							KEYBOARD INPUT: SWORD
	//=======================================================================
	if(input->isKeyDown(SWORD_RIGHT))            // if move right
	{
		keyDownThisFrame = true;
		lastDirection= right;
	}
	if(input->isKeyDown(SWORD_LEFT))             // if move left
	{
		lastDirection = left;
		keyDownThisFrame = true;
	}
	if(input->isKeyDown(SWORD_UP))               // if move up
	{
		lastDirection = up;
		keyDownThisFrame = true;
	}
	if(input->isKeyDown(SWORD_DOWN))             // if move down
	{
		lastDirection = down;
		keyDownThisFrame = true;
	}
	if (!keyDownLastFrame && keyDownThisFrame)
	{
		switch (lastDirection)
		{
		case right:
			crypt.setFrames(CRYPT_RIGHT_SWORD_START, CRYPT_RIGHT_SWORD_END);
			crypt.setCurrentFrame(CRYPT_RIGHT_SWORD_START);
			crypt.flipHorizontal(false);
			audio->playCue(SWORD);
			break;
		case left:
			crypt.setCurrentFrame(CRYPT_RIGHT_SWORD_START);
			crypt.setFrames(CRYPT_RIGHT_SWORD_START, CRYPT_RIGHT_SWORD_END);
			crypt.flipHorizontal(true);
			audio->playCue(SWORD);
			break;
		case up:
			crypt.setCurrentFrame(CRYPT_TOP_SWORD_START);
			crypt.setFrames(CRYPT_TOP_SWORD_START, CRYPT_TOP_SWORD_END);
			crypt.flipVertical(false);
			audio->playCue(SWORD);
			break;
		case down:
			crypt.setCurrentFrame(CRYPT_TOP_SWORD_START);
			crypt.setFrames(CRYPT_TOP_SWORD_START, CRYPT_TOP_SWORD_END);
			crypt.flipVertical(true);
			audio->playCue(SWORD);
			break;
		}
		stop = false;
	}

	if (!keyDownThisFrame)
	{
		crypt.setCurrentFrame(CRYPT_IDLE_START);
		crypt.setFrames(CRYPT_IDLE_START, CRYPT_IDLE_END);
	}
	keyDownLastFrame = keyDownThisFrame;
	keyDownThisFrame = false;

	crypt.update(frameTime);
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void Spacewar::ai()
{}

//=============================================================================
// Handle collisions
//=============================================================================
void Spacewar::collisions()
{}

//=============================================================================
// Render game items
//=============================================================================
void Spacewar::render()
{
    graphics->spriteBegin();                // begin drawing sprites
    crypt.draw();                            // add the spacejpo to the scene
    graphics->spriteEnd();                  // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Spacewar::releaseAll()
{
    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Spacewar::resetAll()
{
    Game::resetAll();
    return;
}
